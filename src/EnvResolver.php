<?php
declare(strict_types=1);

namespace App;

/**
 * Class EnvResolver
 */
class EnvResolver
{
    private string $BaseUrl;
    private string $SearchEndpoint;
    private string $OperationName;
    private string $OperationResponseKey;
    private string $AppName;
    private string $ServiceVersion;
    private string $GlobalId;

    /**
     * EnvResolver constructor.
     */
    public function __construct()
    {
        $this->BaseUrl = $_ENV['BASE_URL'];
        $this->SearchEndpoint = $_ENV['SEARCH_ENDPOINT'];
        $this->OperationName = $_ENV['OPERATION_NAME'];
        $this->OperationResponseKey = $_ENV['OPERATION_NAME'] . 'Response';
        $this->AppName = $_ENV['APP_NAME'];
        $this->ServiceVersion = $_ENV['SERVICE_VERSION'];
        $this->GlobalId = $_ENV['GLOBAL_ID'];
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->BaseUrl;
    }

    /**
     * @return string
     */
    public function getSearchEndpoint(): string
    {
        return $this->SearchEndpoint;
    }

    /**
     * @return string
     */
    public function getOperationName(): string
    {
        return $this->OperationName;
    }

    /**
     * @return string
     */
    public function getOperationResponseKey(): string
    {
        return $this->OperationResponseKey;
    }

    /**
     * @return string
     */
    public function getAppName(): string
    {
        return $this->AppName;
    }

    /**
     * @return string
     */
    public function getServiceVersion(): string
    {
        return $this->ServiceVersion;
    }

    /**
     * @return string
     */
    public function getGlobalId(): string
    {
        return $this->GlobalId;
    }
}
